package com.mal.moviesapp.Database;


public class Contract {

    //movie table
    public static final String M_TABLE_NAME = "MOVIES";
    public static final String M_COLUMN_MOVIE_ID = "M_ID";
    public static final String M_COLUMN_TITLE = "TITLE";
    public static final String M_COLUMN_RATE = "RATE";
    public static final String M_COLUMN_RELEASE_DATE = "DATE";
    public static final String M_COLUMN_PLOTSYNOPSIS = "PLOT";
    public static final String M_COLUMN_PICURL = "PIC";


}
