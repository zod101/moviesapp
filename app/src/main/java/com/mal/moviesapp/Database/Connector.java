package com.mal.moviesapp.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class Connector {
    private SQLiteDatabase db;
    private Helper helper;

    public Connector(Context context) {
        helper = new Helper(context);
    }

    public void open() {
        db = helper.getWritableDatabase();
    }

    public void close() {
        if (db != null) {
            db.close();
        }
    }

    public void insertMovie(String title, String rate, String releaseDate, String plotSynopsis, String picUrl, String apiMovieId) {
        ContentValues newItem = new ContentValues();
        newItem.put(Contract.M_COLUMN_MOVIE_ID, apiMovieId);
        newItem.put(Contract.M_COLUMN_PICURL, picUrl);
        newItem.put(Contract.M_COLUMN_PLOTSYNOPSIS, plotSynopsis);
        newItem.put(Contract.M_COLUMN_RELEASE_DATE, releaseDate);
        newItem.put(Contract.M_COLUMN_TITLE, title);
        newItem.put(Contract.M_COLUMN_RATE, rate);
        db.insert(Contract.M_TABLE_NAME, null, newItem);
    }


    public Cursor getAllMovies() {
        String[] col = {Contract.M_COLUMN_MOVIE_ID, Contract.M_COLUMN_TITLE, Contract.M_COLUMN_RELEASE_DATE,
                Contract.M_COLUMN_PLOTSYNOPSIS, Contract.M_COLUMN_RATE, Contract.M_COLUMN_PICURL};
        return db.query(
                Contract.M_TABLE_NAME, // table name
                col, // column names
                null, // where clause // id param. could be here or appended as it is ^
                null, // where params
                null, // groupby
                null, // having
                null // orderby
        );
    }

    public boolean isMovieExist(String id) {
        String[] col = {Contract.M_COLUMN_MOVIE_ID};

        Cursor cursor = db.query(
                Contract.M_TABLE_NAME, // table name
                col, // column names
                Contract.M_COLUMN_MOVIE_ID + " = '" + id + "'", // where clause // id param. could be here or appended as it is ^
                null, // where params
                null, // groupby
                null, // having
                null // orderby
        );

        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;

    }


    public void deleteMovie(String id) {
        db.delete(
                Contract.M_TABLE_NAME, // table name
                Contract.M_COLUMN_MOVIE_ID + " = " + id, // where clause
                null // where params
        );
    }

}
