package com.mal.moviesapp.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class Helper extends SQLiteOpenHelper {

    private static final String DB_NAME = "movie.db";
    private static final int DB_VERSION = 1;

    private static final String TABLE_MOVIE_CREATE =

            "CREATE TABLE " + Contract.M_TABLE_NAME + "(" +
                    Contract.M_COLUMN_MOVIE_ID + " TEXT, " +
                    Contract.M_COLUMN_PICURL + " TEXT, " +
                    Contract.M_COLUMN_PLOTSYNOPSIS + " TEXT, " +
                    Contract.M_COLUMN_RELEASE_DATE + " TEXT, " +
                    Contract.M_COLUMN_TITLE + " TEXT, " +

                    Contract.M_COLUMN_RATE + " TEXT);";


    public Helper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_MOVIE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Contract.M_TABLE_NAME);
    }
}
