package com.mal.moviesapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MovieDetail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = null;
        if (getIntent().getExtras() != null)
            extras = getIntent().getExtras();

        if (savedInstanceState == null) {
            MovieDetailFragment detailFragment = new MovieDetailFragment();
            detailFragment.setArguments(extras);
            setContentView(R.layout.activity_movie_detail);
            getSupportFragmentManager().beginTransaction().add(R.id.movie_detail_container
                    , detailFragment).commit();
        }
    }
}
