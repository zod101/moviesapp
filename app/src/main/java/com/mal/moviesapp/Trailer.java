package com.mal.moviesapp;

import java.io.Serializable;

public class Trailer implements Serializable {

    private String key;
    private String trailerNAme;

    public String getKey() {
        return key;
    }

    public void setKEY(String key) {
        this.key = key;
    }

    public String getTrailerNAme() {
        return trailerNAme;
    }

    public void setTrailerNAme(String trailerNAme) {
        this.trailerNAme = trailerNAme;
    }
}
