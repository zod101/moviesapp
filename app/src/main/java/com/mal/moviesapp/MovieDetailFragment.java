package com.mal.moviesapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mal.moviesapp.Database.Connector;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.mal.moviesapp.Main.BASE_REVIEW_URL;
import static com.mal.moviesapp.Main.BASE_TRAILER_REVIEW_URL;
import static com.mal.moviesapp.Main.BASE_TRAILER_URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class MovieDetailFragment extends Fragment {
    private static final String YOUTUBE_LINK = "https://www.youtube.com/watch?v=";
    private List<Trailer> listOfTrailers = new ArrayList<>();
    private List<Review> listOfReviews = new ArrayList<>();
    private Connector dbConnecotr;
    private TextView title, rate, releaseDate, plot;
    private ImageView imageView;
    private String id;
    private String pic_url;
    private LinearLayout TrailerlinearLayout;
    private LinearLayout ReviewlinearLayout;
    private Bundle IntentBundle = null;
    public MovieDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_detail, container, false);

        // if(savedInstanceState!=null)
        //{
        //IntentBundle=savedInstanceState.getBundle("ItentData");
        //Log.d("bundle!=null",IntentBundle.getString("title"));
        // }

        title = (TextView) view.findViewById(R.id.title);
        rate = (TextView) view.findViewById(R.id.rate);
        releaseDate = (TextView) view.findViewById(R.id.date);
        plot = (TextView) view.findViewById(R.id.plot);
        imageView = (ImageView) view.findViewById(R.id.movie_pic);
        Button AddFavourite = (Button) view.findViewById(R.id.favourite);
        Button RemoveFavourite = (Button) view.findViewById(R.id.remove);

        TrailerlinearLayout = (LinearLayout) view.findViewById(R.id.trailersLayout);
        ReviewlinearLayout = (LinearLayout) view.findViewById(R.id.reviewsLayout);
        dbConnecotr = new Connector(getContext());
        getData();

        getTrailersAndParse(id);
        getReviewsAndParse(id);

        AddFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbConnecotr.open();
                if (!dbConnecotr.isMovieExist(id)) {
                    dbConnecotr.insertMovie(String.valueOf(title.getText()), String.valueOf(rate.getText())
                            , String.valueOf(releaseDate.getText()), String.valueOf(plot.getText()), pic_url, id);
                    Toast.makeText(getContext(), "Added Successfully!", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(getContext(), "this Movie is already in favourite", Toast.LENGTH_LONG).show();
                dbConnecotr.close();
            }
        });

        RemoveFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbConnecotr.open();
                if (dbConnecotr.isMovieExist(id)) {

                    dbConnecotr.deleteMovie(id);
                    Toast.makeText(getContext(), "Removed Successfully!", Toast.LENGTH_LONG).show();

                } else
                    Toast.makeText(getContext(), "this Movie isn't in favourites", Toast.LENGTH_LONG).show();
                dbConnecotr.close();

            }
        });
        return view;
    }

    public void getData() {
        if (getArguments().getBundle("bundle") != null) {
            IntentBundle = getArguments().getBundle("bundle");
            title.setText(IntentBundle.getString("title"));
            rate.setText(IntentBundle.getString("rate"));
            releaseDate.setText(IntentBundle.getString("date"));
            plot.setText(IntentBundle.getString("plot"));
            id = IntentBundle.getString("id");
            pic_url = IntentBundle.getString("url");
            Picasso.with(getContext()).load(IntentBundle.getString("url")).fit().placeholder(R.drawable.holder).into(imageView);
        }
        }

    public void getTrailersAndParse(String id) {
        Log.d("id", id);
        Ion.with(this)
                .load(BASE_TRAILER_REVIEW_URL + id + BASE_TRAILER_URL)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e != null) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        parseTrailerJson(result);
                    }
                });
    }

    public void getReviewsAndParse(String id) {
        Ion.with(this)
                .load(BASE_TRAILER_REVIEW_URL + id + BASE_REVIEW_URL)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e != null) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Log.d("review", result);
                        parseReviewsJson(result);
                    }
                });
    }


    public void parseReviewsJson(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray results = jsonObject.getJSONArray("results");

            for (int i = 0; i < results.length(); i++) {
                JSONObject trailerObj = results.getJSONObject(i);
                String author = trailerObj.getString("author");
                String content = trailerObj.getString("content");
                Review review = new Review(author, content);

                Log.d("trailers", review.getAuthor());
                listOfReviews.add(review);
            }
            addReviewViewsToLinearLayout();
        } catch (JSONException t) {
            t.printStackTrace();
        }
    }

    public void addReviewViewsToLinearLayout() {
        for (int i = 0; i < listOfReviews.size(); i++) {
            View item = LayoutInflater.from(getContext()).inflate(R.layout.review_item, null);
            TextView author = (TextView) item.findViewById(R.id.author);
            TextView content = (TextView) item.findViewById(R.id.review_content);

            author.setText(listOfReviews.get(i).getAuthor());
            content.setText(listOfReviews.get(i).getContent());

            ReviewlinearLayout.addView(item);

        }

    }



    public void parseTrailerJson(String result)
    {
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray results = jsonObject.getJSONArray("results");

            for (int i = 0; i < results.length(); i++) {
                Trailer trailer = new Trailer();
                JSONObject trailerObj = results.getJSONObject(i);
                String title = trailerObj.getString("name");
                String key = trailerObj.getString("key");
                trailer.setTrailerNAme(title);
                trailer.setKEY(key);
                Log.d("trailers", trailer.getTrailerNAme());
                listOfTrailers.add(trailer);
            }
            addTrailerViewsToLinearLayout();
        } catch (JSONException t) {
            t.printStackTrace();
        }
    }

    public void addTrailerViewsToLinearLayout()
    {
        for (int i = 0; i < listOfTrailers.size(); i++) {
            View item = LayoutInflater.from(getContext()).inflate(R.layout.linear_layout_item,null);
            TextView trailerTitle = (TextView) item.findViewById(R.id.trailerTitleTV);
            trailerTitle.setText(listOfTrailers.get(i).getTrailerNAme());
            TrailerlinearLayout.addView(item);

            final int finalI = i;
            trailerTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(YOUTUBE_LINK + listOfTrailers.get(finalI).getKey())));
                }

            });
        }

    }

}
