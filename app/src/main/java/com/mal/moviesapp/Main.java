package com.mal.moviesapp;


import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mal.moviesapp.Database.Connector;
import com.mal.moviesapp.Database.Contract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Main extends Fragment {
    public static final String API_KEY = "0ad818053218184a4f89716fa901d427";
    public static final String BASE_TRAILER_REVIEW_URL = "http://api.themoviedb.org/3/movie/";
    public static final String BASE_TRAILER_URL = "/videos?api_key=" + API_KEY;
    public static final String BASE_REVIEW_URL = "/reviews?api_key=" + API_KEY;
    private List<MovieModel> movies =new ArrayList<>();
    private CustomAdapter customAdapter;
    private Connector databaseConnector;
    private String sortby;
    private TransformInterface anInterface;
    public Main() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            sortby = savedInstanceState.getString("setting");
        } else {
            sortby = PreferenceManager.getDefaultSharedPreferences(getActivity())
                    .getString(getString(R.string.key_pref_movie_type), getString(R.string.popular));
        }

        databaseConnector = new Connector(getContext());
        customAdapter = new CustomAdapter(getActivity().getApplicationContext(), movies);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        if (isConnectedToInternet()) {


            GridView gridView = (GridView) rootView.findViewById(R.id.gridView);


            if (sortby.equals(getString(R.string.popular))) {
                getDataFromApiIfPopular();
            } else if (sortby.equals(getString(R.string.toprated))) {
                getDataFromApiIfTopRated();
            } else if (sortby.equals(getString(R.string.favourite))) {
                getDataFromDatabaseAndParseIt();
            }
            gridView.setAdapter(customAdapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    // Intent intent = new Intent(getActivity(), MovieDetail.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("title", movies.get(i).getTitle());
                    bundle.putString("rate", movies.get(i).getRate());
                    bundle.putString("date", movies.get(i).getReleaseDate());
                    bundle.putString("plot", movies.get(i).getPlotSynopsis());
                    bundle.putString("url", movies.get(i).getPicUrl());
                    //Log.d("main", movies.get(i).getTitle() + " " + movies.get(i).getPicUrl());
                    bundle.putString("id", movies.get(i).get_id());
                    Log.d("mainID", movies.get(i).get_id());
                    anInterface.transfareData(bundle);
                }
            });
            return rootView;
        } else
            Toast.makeText(getContext(), "please connect to the internet to get data", Toast.LENGTH_LONG).show();
        return null;
    }

    public void setTransfromInterface(TransformInterface transfromInterface) {
        anInterface = transfromInterface;
    }

    @Override
    public void onResume() {

        String sortBy = PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString(getString(R.string.key_pref_movie_type), getString(R.string.popular));

        if (sortBy.equals(getString(R.string.favourite))) {
            getDataFromDatabaseAndParseIt();
        } else if (sortBy.equals(getString(R.string.popular)))
        {
            getDataFromApiIfPopular();
        }
        else if (sortBy.equals(getString(R.string.toprated)))
        {
            getDataFromApiIfTopRated();
        }
        super.onResume();
    }

    private void getDataFromApiIfTopRated() {
        Ion.with(this)
                .load("http://api.themoviedb.org/3/movie/top_rated?api_key=" + API_KEY)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e != null)
                        {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Log.d("fragment",result);
                        movies.clear();
                        movies =parseJson(result);
                        customAdapter.setMovies(movies);
                        //customAdapter.notifyDataSetChanged();
                    }
                });
    }

    private void getDataFromApiIfPopular() {

        Ion.with(this)
                .load("http://api.themoviedb.org/3/movie/popular?api_key=" + API_KEY)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e != null)
                        {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Log.d("fragment",result);
                        movies.clear();
                        movies =parseJson(result);
                        customAdapter.setMovies(movies);
                        //customAdapter.notifyDataSetChanged();
                    }
                });
    }

    private List<MovieModel> parseJson(String jsonStr) {
        List<MovieModel> listOfMovies = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray results = jsonObject.getJSONArray("results");

            for (int i = 0; i < results.length(); i++) {
                MovieModel movie = new MovieModel();
                JSONObject movieObj = results.getJSONObject(i);
                String title = movieObj.getString("original_title");
                String rate = movieObj.getString("vote_average");
                String releaseDate = movieObj.getString("release_date");
                String baseURL = "https://image.tmdb.org/t/p/w500/";
                String AlbumPicUrl = baseURL + movieObj.getString("poster_path");
                String plot = movieObj.getString("overview");
                String _id = movieObj.getString("id");
                movie.setTitle(title);
                movie.setRate(rate);
                movie.setReleaseDate(releaseDate);
                movie.setPicUrl(AlbumPicUrl);
                movie.setPlotSynopsis(plot);
                movie.set_id(_id);
                Log.d("fragment 4 loop", movie.getTitle());
                listOfMovies.add(movie);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listOfMovies;
    }


    public void getDataFromDatabaseAndParseIt() {
        databaseConnector.open();
        Cursor result = databaseConnector.getAllMovies();
        if (result != null && result.getCount() > 0) {
            movies.clear();
            try {
                if (result.moveToFirst()) {
                    int index_id = result.getColumnIndex(Contract.M_COLUMN_MOVIE_ID);
                    int index_picUrl = result.getColumnIndex(Contract.M_COLUMN_PICURL);
                    int index_title = result.getColumnIndex(Contract.M_COLUMN_TITLE);
                    int index_rate = result.getColumnIndex(Contract.M_COLUMN_RATE);
                    int index_Plot = result.getColumnIndex(Contract.M_COLUMN_PLOTSYNOPSIS);
                    int index_Date = result.getColumnIndex(Contract.M_COLUMN_RELEASE_DATE);
                    do {
                        MovieModel movieModel = new MovieModel();
                        movieModel.set_id(result.getString(index_id));
                        movieModel.setPicUrl(result.getString(index_picUrl));
                        movieModel.setPlotSynopsis(result.getString(index_Plot));
                        movieModel.setReleaseDate(result.getString(index_Date));
                        movieModel.setRate(result.getString(index_rate));
                        movieModel.setTitle(result.getString(index_title));
                        Log.d("datafromdb", result.getString(index_title));
                        movies.add(movieModel);
                    } while (result.moveToNext());
                    databaseConnector.close();
                    customAdapter.setMovies(movies);
                }
            } catch (Exception e) {
                Toast.makeText(getContext(), e.getMessage(),
                        Toast.LENGTH_LONG).show();

            }

        } else {
            Toast.makeText(getContext(), "No movies in favourites yet.", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        String sortby = PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString(getString(R.string.key_pref_movie_type), getString(R.string.popular));

        savedInstanceState.putString("setting", sortby);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public boolean isConnectedToInternet() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


}
