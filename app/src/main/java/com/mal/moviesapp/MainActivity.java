package com.mal.moviesapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements TransformInterface {

    private Boolean TwoPain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TwoPain = findViewById(R.id.movie_detail_container) != null;

        if (savedInstanceState == null)
        {
            Main main = new Main();
            main.setTransfromInterface(this);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, main).commit();
        }
    }

    @Override
    public void transfareData(Bundle bundle) {
        if (TwoPain) {
            Bundle data = new Bundle();
            data.putBundle("bundle", bundle);
            MovieDetailFragment detailFragment = new MovieDetailFragment();
            detailFragment.setArguments(data);
            getSupportFragmentManager().beginTransaction().replace(R.id.movie_detail_container, detailFragment).commit();
        } else {
            Intent i = new Intent(this, MovieDetail.class);
            i.putExtra("bundle", bundle);
            startActivity(i);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_item_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

}
